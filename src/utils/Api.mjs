export const response = async (res, data) => {
  res.json(data)
}

export const error = (res, message, code = 500) => {
  res.status(code)
  res.json({
    error: {
      code,
      message,
    },
  })
}
