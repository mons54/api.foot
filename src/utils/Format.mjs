export const formatMatch = (match) => {
  const data = {
    id: match.id,
    day: match.day,
    played: false,
    team_home: {
      id: match.id_team_home,
      name: match.nth,
      short_name: match.sth,
    },
    team_away: {
      id: match.id_team_away,
      name: match.nta,
      short_name: match.sta,
    },
  }

  if (match.date)
    data.date = match.date

  if (match.score_home !== null) {
    data.played = true
    data.team_home.score = match.score_home
    data.team_away.score = match.score_away
  }

  return data
}

export const formatMatchs = (matchs) => {
  const response = []
  matchs.forEach(match => response.push(formatMatch(match)))
  return response
}
