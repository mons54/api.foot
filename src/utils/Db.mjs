import mysql from 'mysql'

export const connection = () => {
  const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'football_french_championship'
  })
  connection.config.queryFormat = function (query, values) {
    if (!values) return query;
    return query.replace(/\:(\w+)/g, (txt, key) => {
      if (values.hasOwnProperty(key))
        return this.escape(values[key])
      return txt
    })
  }
  return connection
}

export const query = (req, params = {}) => new Promise(res => {
  const db = connection()
  db.query(req, params, (err, data) => {
    if (err) console.error(err)
    res(data)
  })
  db.end()
})

export const fetchAll = query

export const fetch = (req, params = {}) => new Promise(res => {
  const db = connection()
  db.query(req, params, (err, data) => {
    if (err) console.error(err)
    res(data ? data[0] : null)
  })
  db.end()
})
