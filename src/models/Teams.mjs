import {fetch, fetchAll} from '../utils/Db'

const req = `SELECT * FROM teams`

export default class {

  static getTeam(id) {
    return fetch(`${req} WHERE id = :id`, {
      id
    })
  }

  static getTeams() {
    return fetchAll(req)
  }
}
