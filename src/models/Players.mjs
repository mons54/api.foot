import {fetch, fetchAll} from '../utils/Db'

const req = `
  SELECT p.*, t.id AS id_team
  FROM players AS p
  INNER JOIN players_has_teams AS pht
  ON pht.id_player = p.id
  INNER JOIN teams AS t
  ON pht.id_team = t.id
`

export default class {

  static getPlayer(id) {
    return fetch(`${req} WHERE p.id = :id`, {
      id
    })
  }

  static getPlayersByTeam(team) {
    return fetchAll(`${req} WHERE pht.id_team = :team`, {
      team
    })
  }
}
