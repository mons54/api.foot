import {fetchAll} from '../utils/Db'

export default class {

  static getRanking(start, end) {
    let conditions = ''
    let params = {}
    if (start) {
      conditions += ` AND day >= :start`
      params.start = start
    }
    if (end) {
      conditions += ` AND day <= :end`
      params.end = end
    }
    return fetchAll(`
      SELECT id_team_home as id, teams.name,
      SUM(CASE WHEN result = 1 THEN 3 WHEN result = 0 THEN 1 ELSE 0 END) AS points,
      COUNT(result) AS played,
      COUNT(CASE WHEN result = 1 THEN result END) won,
      COUNT(CASE WHEN result = 0 THEN result END) draw,
      COUNT(CASE WHEN result = -1 THEN result END) lost,
      SUM(gf) AS gf,
      SUM(ga) AS ga,
      SUM(gf-ga) AS gd
      FROM (
        SELECT id_team_home, score_home AS gf, score_away AS ga,
        CASE WHEN score_home > score_away THEN 1 WHEN score_home = score_away THEN 0 ELSE -1 END result
        FROM matchs
        WHERE score_home IS NOT NULL ${conditions}
        UNION ALL
        SELECT id_team_away, score_away AS gf, score_home AS ga,
        CASE WHEN score_home < score_away THEN 1 WHEN score_home = score_away THEN 0 ELSE -1 END result
        FROM matchs
        WHERE score_home IS NOT NULL ${conditions}
      ) mm
      LEFT JOIN teams ON teams.id = id_team_home
      GROUP BY id_team_home, name
      ORDER BY points DESC, gd DESC, gf DESC, played ASC
    `, params)
  }
}
