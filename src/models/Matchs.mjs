import {fetch, fetchAll} from '../utils/Db'

const req = `
  SELECT m.*, th.name AS nth, ta.name AS nta,
  th.short_name AS sth, ta.short_name AS sta
  FROM matchs AS m
  INNER JOIN teams AS th
  ON m.id_team_home = th.id
  INNER JOIN teams AS ta
  ON m.id_team_away = ta.id
  WHERE m.id_season = 1
`

export default class {

  static getMatch(id) {
    return fetch(`${req} AND m.id = :id`, {
      id
    })
  }

  static getMatchs(day) {
    return fetchAll(`${req} AND m.day = :day`, {
      day
    })
  }

  static getMatchsByTeam(team) {
    return fetchAll(`${req} AND (th.id = :team OR ta.id = :team)`, {
      team
    })
  }
}
