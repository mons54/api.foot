import {fetch, fetchAll} from '../utils/Db'

const req = `
  SELECT c.*, t.id AS id_team
  FROM coachs AS c
  INNER JOIN coachs_has_teams AS cht
  ON cht.id_coach = c.id
  INNER JOIN teams AS t
  ON cht.id_team = t.id
`

export default class {

  static getCoach(id) {
    return fetch(`${req} WHERE c.id = :id`, {
      id
    })
  }

  static getCoachs() {
    return fetchAll(req)
  }

  static getCoachByTeam(team) {
    return fetch(`${req} WHERE cht.id_team = :team`, {
      team
    })
  }
}
