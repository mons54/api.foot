import {fetch, fetchAll} from '../utils/Db'

const req = `
  SELECT s.*, t.id AS id_team
  FROM stadiums AS s
  INNER JOIN teams AS t ON t.id_stadium = s.id`

export default class {

  static getStadium(id) {
    return fetch(`${req} WHERE s.id = :id`, {
      id
    })
  }

  static getStadiums() {
    return fetchAll(req)
  }
}
