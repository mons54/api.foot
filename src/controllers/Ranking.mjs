import Ranking from '../models/Ranking'
import {response} from '../utils/Api'

export default class {

  static async getRanking(req, res) {
    let start = parseInt(req.query.start) || null
    let end = parseInt(req.query.end) || null
    if (!start || start < 1 || start > 38) {
      start = null
      if (end > 0 && end < 39) {
        start = 1
      } else {
        end = null
      }
    } else if (end < start || end > 38) {
      end = 38
    }
    response(res, await Ranking.getRanking(start, end))
  }
}
