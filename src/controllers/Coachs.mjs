import Coachs from '../models/Coachs'
import {response, error} from '../utils/Api'

export default class {
  
  static async getCoach(req, res) {
    const coach = await Coachs.getCoach(req.params.id)
    if (!coach)
      return error(res, "Coach ID invalid", 406)
    response(res, coach)
  }

  static async getCoachs(req, res) {
    response(res, await Coachs.getCoachs());
  }
}
