import Coachs from '../models/Coachs'
import Matchs from '../models/Matchs'
import Players from '../models/Players'
import Teams from '../models/Teams'
import {response, error} from '../utils/Api'
import {formatMatchs} from '../utils/Format'

export default class {

  static async getTeams(req, res) {
    response(res, await Teams.getTeams());
  }

  static async getTeam(req, res) {
    const team = await Teams.getTeam(req.params.id)
    if (!team)
      return error(res, "Team ID invalid", 406)
    response(res, team)
  }

  static async getCoach(req, res) {
    const coach = await Coachs.getCoachByTeam(req.params.id)
    if (!coach)
      return error(res, "Team ID invalid", 406)
    response(res, coach)
  }

  static async getPlayers(req, res) {
    const players = await Players.getPlayersByTeam(req.params.id)
    if (!players || !players.length)
      return error(res, "Team ID invalid", 406)
    response(res, players)
  }

  static async getMatchs(req, res) {
    const matchs = await Matchs.getMatchsByTeam(req.params.id)
    if (!matchs || !matchs.length)
      return error(res, "Team ID invalid", 406)
    response(res, formatMatchs(matchs))
  }
}
