import Matchs from '../models/Matchs'
import {response, error} from '../utils/Api'
import {formatMatch, formatMatchs} from '../utils/Format'

export default class {

  static async getMatch(req, res) {
    const match = await Matchs.getMatch(req.params.id)
    if (!match)
      return error(res, "Match ID invalid", 406)
    response(res, formatMatch(match))
  }

  static async getMatchs(req, res) {
    const matchs = await Matchs.getMatchs(req.params.day)
    if (!matchs || !matchs.length)
      return error(res, "Day invalid", 406)
    response(res, formatMatchs(matchs))
  }
}
