import Players from '../models/Players'
import {response, error} from '../utils/Api'

export default class {

  static async getPlayer(req, res) {
    const player = await Players.getPlayer(req.params.id)
    if (!player)
      return error(res, "Player ID invalid", 406)
    response(res, player)
  }
}
