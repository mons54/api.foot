import Stadiums from '../models/Stadiums'
import {response, error} from '../utils/Api'

export default class {

  static async getStadium(req, res) {
    const stadium = await Stadiums.getStadium(req.params.id)
    if (!stadium)
      return error(res, "Stadium ID invalid", 406)
    response(res, stadium)
  }

  static async getStadiums(req, res) {
    response(res, await Stadiums.getStadiums());
  }
}
