import express from 'express'
import cors from 'cors'
import memoryCache from 'memory-cache'

import {error} from './src/utils/Api'
import Coachs from './src/controllers/Coachs'
import Matchs from './src/controllers/Matchs'
import Players from './src/controllers/Players'
import Ranking from './src/controllers/Ranking'
import Stadiums from './src/controllers/Stadiums'
import Teams from './src/controllers/Teams'

const app = express()

app.use(cors({
  origin: '*'
}))

app.use((req, res, next) => {
  if (res.sendJson)
    return next()
  const key = req.url
  const data = memoryCache.get(key)
  if (data) {
    res.json(data)
  } else {
    res.sendJson = res.json
    res.json = (data) => {
      memoryCache.put(key, data, 3600000)
      res.sendJson(data)
    }
    next()
  }
})

/**
 * @api {get} /coachs Aficher la liste des entraineurs
 * @apiName GetCoachs
 * @apiGroup Entraineurs
 * @apiSampleRequest coachs
 */
app.get('/coachs', Coachs.getCoachs)

/**
 * @api {get} /coachs/:id Afficher un entraineur
 * @apiName GetCoach
 * @apiGroup Entraineurs
 * @apiParam {Number} id ID de l'entraineur
 * @apiSampleRequest coachs/:id
 */
app.get('/coachs/:id([0-9]+)', Coachs.getCoach)

/**
 * @api {get} /matchs/:id Afficher un match
 * @apiName GetMatch
 * @apiGroup Matchs
 * @apiParam {Number} id ID du match
 * @apiSampleRequest matchs/:id
 */
app.get('/matchs/:id([0-9]+)', Matchs.getMatch)

/**
 * @api {get} /matchs/day/:day Afficher les match d'une journée
 * @apiName GetMatchsDay
 * @apiGroup Matchs
 * @apiParam {Number} day Numéro de la journée
 * @apiSampleRequest matchs/day/:day
 */
app.get('/matchs/day/:day([0-9]+)', Matchs.getMatchs)

/**
 * @api {get} /players/:id Afficher un joueur
 * @apiName GetPlayer
 * @apiGroup Joueurs
 * @apiParam {Number} id ID du joueur
 * @apiSampleRequest players/:id
 */
app.get('/players/:id([0-9]+)', Players.getPlayer)

/**
 * @api {get} /ranking?start=:start&end=:end Aficher le classement
 * @apiName GetRanking
 * @apiGroup Classement
 * @apiSampleRequest ranking?start=:start&end=:end
 * @apiParam {Number} start Journée de début
 * @apiParam {Number} end Journée de fin
 */
app.get('/ranking', Ranking.getRanking)

/**
 * @api {get} /stadiums Afficher la liste des stades
 * @apiName GetStadiums
 * @apiGroup Stades
 * @apiSampleRequest stadiums
 */
app.get('/stadiums', Stadiums.getStadiums)

/**
 * @api {get} /stadiums/:id Afficher un stade
 * @apiName GetStadium
 * @apiGroup Stades
 * @apiParam {Number} id ID du stade
 * @apiSampleRequest stadiums/:id
 */
app.get('/stadiums/:id([0-9]+)', Stadiums.getStadium)

/**
 * @api {get} /teams Afficher la liste des équipes
 * @apiName GetTeams
 * @apiGroup Equipes
 * @apiSampleRequest teams
 */
app.get('/teams', Teams.getTeams)

/**
 * @api {get} /teams/:id Afficher une équipe
 * @apiName GetTeam
 * @apiGroup Equipes
 * @apiParam {Number} id ID de l'équipe
 * @apiSampleRequest teams/:id
 */
app.get('/teams/:id([0-9]+)', Teams.getTeam)

/**
 * @api {get} /teams/:id/coach Afficher l'entraineur d'une équipe
 * @apiName GetTeamCoach
 * @apiGroup Equipes
 * @apiParam {Number} id ID de l'équipe
 * @apiSampleRequest teams/:id/coach
 */
app.get('/teams/:id([0-9]+)/coach', Teams.getCoach)

/**
 * @api {get} /teams/:id/players Afficher les joueurs d'une équipe
 * @apiName GetTeamPlayers
 * @apiGroup Equipes
 * @apiParam {Number} id ID de l'équipe
 * @apiSampleRequest teams/:id/players
 */
app.get('/teams/:id([0-9]+)/players', Teams.getPlayers)

/**
 * @api {get} /teams/:id/matchs Afficher les matchs d'une équipe
 * @apiName GetTeamMatchs
 * @apiGroup Equipes
 * @apiParam {Number} id ID de l'équipe
 * @apiSampleRequest teams/:id/matchs
 */
app.get('/teams/:id([0-9]+)/matchs', Teams.getMatchs)

app.use(express.static('docs'))

app.all('*', (req, res) => {
  error(res, "The requested URL was not found on the server.")
})

app.listen(3000)
